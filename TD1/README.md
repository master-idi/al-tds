# TD1 - Refactoring

## Exercice 1 : Reverse engeneering

*Décrivez les différentes classes disponibles et les services qu'elles offrent :*

`Customers.java` :

- permet de créer un client à partir de son nom,
- possède une liste d'emprunts,
- permet l'ajout d'un emprunt dans la liste,
- permet la récuperation du nom,
- permet la récuperation de l'état des emprunt

`Movie.java` :

- permet de créer un film à partir de son titre et du code de son prix,
- permet de récupérer et de modifier sont code de prix,
- permet de récupérer son titre

`Rental.java` :
- permet de récupérer un emprunt à partir d'un `Movie` et d'un nombre de jour d'emprunt,
- permet de récupérer le `Movie`,
- permet de récupérer le nombre de jours d'emprunt

*Décrivez les interactions entre vos objets pendant l'exécution de ce programme :*

__TODO__

## Exercice 2 : Test de non regression

```java
@Test
    public static void main(String[] args) {

        Movie rogueOne = new Movie("Rogue One", 1);
        Movie frozen = new Movie("Reine des neiges", 2);
        Movie revengeOfTheSith = new Movie("Star Ward III", 0);

        Customers customer = new Customers("John Doe");

        customer.addRental(new Rental(rogueOne, 5));
        customer.addRental(new Rental(frozen, 7));
        customer.addRental(new Rental(revengeOfTheSith, 4));

        String result = customer.statement();

        assertEquals("Rental Record for John Doe\n" +
                "\tRogue One\t15.0 \n" +
                "\tReine des neiges\t7.5 \n" +
                "\tStar Ward III\t5.0 \n" +
                "Amount owned is 27.5\n" +
                "You earned 4 frequent renter points", result);
    }
```

## Exercie 3 : Critiques

*Critiquer l'état du code de l'application :*

Le code actuel n'est pas très maintenable, évolutif et lisible.

*Établir une liste des améliorations nécessaires selon les critères suivants: Lisibilité, Maintenabilité, Réutilisabilité et Extensibilité :*


__Lisibilité__ :
- indentation à revoir,
- méthode `statement()` dans `Customers` à scinder en plusieurs fonction,
- l'utilisation d'une `ArrayList<Rental>` plutôt qu'un `Vector`,
- `Customers` => `Customer`,
- `CHILDRENS` => `CHILDREN`

__Maintenabilité__ :
- supprimer `setPriceCode()` pour l'encapsulation

__Réutilisabilité__ :
- il faudrait abstraire `priceCode` pour pouvoir l'utiliser dans une autre classe (comme `Show` par exemple)

__Extensibilité__ :
- abstraire les calculs de prix d'emprunt en fonction du type de `priceCode` pour l'utiliser dans une autre classe
