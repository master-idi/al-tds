import model.Customers;
import model.Movie;
import model.Rental;

public class Main {

    public static void main(String[] args) {

        Movie rogueOne = new Movie("Rogue One", 1);
        Movie frozen = new Movie("Reine des neiges", 2);
        Movie revengeOfTheSith = new Movie("Star Ward III", 0);

        Customers customer = new Customers("John Doe");

        customer.addRental(new Rental(rogueOne, 5));
        customer.addRental(new Rental(frozen, 7));
        customer.addRental(new Rental(revengeOfTheSith, 4));

        System.out.println(customer.statement());

    }
}
