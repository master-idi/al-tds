package model;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CustomersTest {

    Customers customer;
    @BeforeEach
    void setUp() {

        Movie rogueOne = new Movie("Rogue One", 1);
        Movie frozen = new Movie("Reine des neiges", 2);
        Movie revengeOfTheSith = new Movie("Star Ward III", 0);

        customer = new Customers("John Doe");

        customer.addRental(new Rental(rogueOne, 5));
        customer.addRental(new Rental(frozen, 7));
        customer.addRental(new Rental(revengeOfTheSith, 4));

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void statement() {
        assertEquals("Rental Record for John Doe\n" +
                "\tRogue One\t15.0 \n" +
                "\tReine des neiges\t7.5 \n" +
                "\tStar Ward III\t5.0 \n" +
                "Amount owned is 27.5\n" +
                "You earned 4 frequent renter points", customer.statement());
    }

    @Test
    void statementNotEqual() {
        assertNotEquals("Rental Record for Aurelien Satger\n" +
                "\tRogue One\t15.0 \n" +
                "\tReine des neiges\t7.5 \n" +
                "\tStar Ward III\t5.0 \n" +
                "Amount owned is 27.5\n" +
                "You earned 4 frequent renter points", customer.statement());
    }
}