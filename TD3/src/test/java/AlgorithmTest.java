import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest {

    Algorithm fibonacci;
    Algorithm padovan;
    private final int iteration = 10;

    @BeforeEach
    void setUp() {
        this.fibonacci = new Fibonacci();
        this.padovan = new Padovan();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void fibonacciWithCache() {
        Cache cache = new Cache(this.fibonacci);
        assertEquals(this.fibonacci.getVal(this.iteration), cache.getVal(this.iteration));
        assertEquals(this.fibonacci.getVal(this.iteration), cache.getVal(this.iteration));
    }

    @Test
    void padovanWithCache() {
        Cache cache = new Cache(this.padovan);
        assertEquals(this.padovan.getVal(this.iteration), cache.getVal(this.iteration));
        assertEquals(this.padovan.getVal(this.iteration), cache.getVal(this.iteration));
    }
}
