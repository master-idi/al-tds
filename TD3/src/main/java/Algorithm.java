public interface Algorithm {

    String getName();

    Double getVal(int i);
}
