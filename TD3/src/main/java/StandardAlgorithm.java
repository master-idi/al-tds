
public class StandardAlgorithm implements AlgorithmFactory {

	@Override
	public Algorithm fibonacci() {
		return new Fibonacci();
	}
	@Override
	public Algorithm padovan() {
		return new Padovan();
	}

}
