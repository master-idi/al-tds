public class Padovan implements Algorithm {

    public Padovan() {
    }

    @Override
    public String getName() {
        return "Padovan";
    }

    @Override
    public Double getVal(int i) {
        if (i < 3) return 1.;
        return getVal(i - 2) + getVal(i - 3);
    }
}
