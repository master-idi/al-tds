public class Main {

    public static void main(String[] args) {

        Algorithm fiboCache = new Cache(new Fibonacci());
        Algorithm padovanCache = new Cache(new Padovan());
        System.out.println("=== " + fiboCache.getName() + " ===");
        System.out.println(fiboCache.getVal(2));

        System.out.println('\n');

        System.out.println("=== " + padovanCache.getName() + " ===");
        System.out.println(padovanCache.getVal(10));

        System.out.println("=== FACTORY ===");

        AlgorithmFactory factory = new CachedAlgorithm();
        System.out.println("=== " + factory.fibonacci().getName() + " ===");
        System.out.println(factory.fibonacci().getVal(10));

    }
}
