import java.util.HashMap;

public class Cache implements Algorithm {

    private final Algorithm _delegate;
    private final HashMap<Integer, Double> _cache;

    public Cache(Algorithm delegate) {
        this._delegate = delegate;
        this._cache = new HashMap<>();
    }

    @Override
    public String getName() {
        return this._delegate.getName();
    }

    @Override
    public Double getVal(int i) {
        if (!this._cache.containsKey(i))
            this._cache.put(i, this._delegate.getVal(i));
        return this._cache.get(i);
    }
}
