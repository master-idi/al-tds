
public class CachedAlgorithm implements AlgorithmFactory {

	@Override
	public Algorithm fibonacci() {
		return new Cache(new Fibonacci());
	}

	@Override
	public Algorithm padovan() {
		return new Cache(new Padovan());
	}
	
}
