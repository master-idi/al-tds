public interface AlgorithmFactory {

    Algorithm fibonacci();
    Algorithm padovan();
}
