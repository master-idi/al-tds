import java.util.HashMap;

public class Fibonacci implements Algorithm {

    public Fibonacci() {
    }

    @Override
    public String getName() {
        return "Fibonacci";
    }

    @Override
    public Double getVal(int i) {
        if (i < 2) return 1.;
        return getVal(i - 1) + getVal(i - 2);
    }
}
