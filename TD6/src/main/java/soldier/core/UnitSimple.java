package soldier.core;

public abstract class UnitSimple {

    private BehaviorSoldier _behaviorSoldier;
    private int weaponsNumber = 0;

    protected UnitSimple(BehaviorSoldier behaviorSoldier) {
        this._behaviorSoldier = behaviorSoldier;
    }

    public float parry(float force) {
        return this._behaviorSoldier.parry(force);
    }

    public float strike() {
        return this._behaviorSoldier.strike();
    }

    public void addSword() throws ImpossibleExtensionException {
        if(this.weaponsNumber > 2)
            throw new ImpossibleExtensionException();
        this._behaviorSoldier = new StdExtension(20, 20, _behaviorSoldier);
        ++this.weaponsNumber;
    }

    public void addShield() throws ImpossibleExtensionException {
        if(this.weaponsNumber > 2)
            throw new ImpossibleExtensionException();
        this._behaviorSoldier = new StdExtension(5, 15, _behaviorSoldier);
        ++this.weaponsNumber;
    }

    public void removeSword() {

    }

    public void removeShield() {

    }
}
