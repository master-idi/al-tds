package soldier;

public abstract class Soldier implements ISoldier {

    protected int healthPoints;

    public Soldier(int healthPoints) {
        this.healthPoints = healthPoints;
    }
}
