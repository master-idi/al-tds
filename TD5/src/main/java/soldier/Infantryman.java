package soldier;

public class Infantryman extends Soldier {

    private final int _strikeDamages = 10;
    private final int _resistanceCapacity = 2;

    public Infantryman(int lifePoints) {
        super(lifePoints);
    }

    @Override
    public boolean isAlive() {
        return this.healthPoints > 0;
    }

    @Override
    public void parry(int damage) {
        this.healthPoints -= (damage - this._resistanceCapacity);
    }

    @Override
    public int strike() {
        return this._strikeDamages;
    }

    @Override
    public int getHealthPoints() {
        return this.healthPoints;
    }

    @Override
    public String getName() {
        return "Infantryman";
    }
}
