package soldier;

public abstract class SoldierDecorator implements ISoldier {

    ISoldier _soldier;

    public SoldierDecorator(ISoldier ISoldier) {
        this._soldier = ISoldier;
    }
}
