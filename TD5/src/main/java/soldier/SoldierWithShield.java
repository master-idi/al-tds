package soldier;

public class SoldierWithShield extends SoldierDecorator {

    private final int _strikeDamages = 2;
    private final int _resistanceCapacity = 15;

    public SoldierWithShield(ISoldier soldier) {
        super(soldier);
    }

    @Override
    public boolean isAlive() {
        return this._soldier.isAlive();
    }

    @Override
    public void parry(int damage) {
        this._soldier.parry(damage - this._resistanceCapacity);
    }

    @Override
    public int strike() {
        return this._soldier.strike() + this._strikeDamages;
    }

    @Override
    public int getHealthPoints() {
        return this._soldier.getHealthPoints();
    }

    @Override
    public String getName() {
        return this._soldier.getName() + " with *shield*";
    }
}
