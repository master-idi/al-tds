package soldier;

public class Horseman extends Soldier {

    private final int _strikeDamages = 6;
    private final int _resistanceCapacity = 4;

    public Horseman(int lifePoints) {
        super(lifePoints);
    }

    @Override
    public boolean isAlive() {
        return this.healthPoints > 0;
    }

    @Override
    public void parry(int damage) {
        this.healthPoints -= (damage - this._resistanceCapacity);
    }

    @Override
    public int strike() {
        return this._strikeDamages;
    }

    @Override
    public int getHealthPoints() {
        return this.healthPoints;
    }

    @Override
    public String getName() {
        return "Horseman";
    }
}
