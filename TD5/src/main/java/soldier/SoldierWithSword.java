package soldier;

public class SoldierWithSword extends SoldierDecorator {

    private final int _strikeDamages = 15;
    private final int _resistanceCapacity = 2;

    public SoldierWithSword(ISoldier soldier) {
        super(soldier);
    }

    @Override
    public boolean isAlive() {
        return this._soldier.isAlive();
    }

    @Override
    public void parry(int damage) {
        this._soldier.parry(damage - this._resistanceCapacity);
    }

    @Override
    public int strike() {
        return this._soldier.strike() + this._strikeDamages;
    }

    @Override
    public int getHealthPoints() {
        return this._soldier.getHealthPoints();
    }

    @Override
    public String getName() {
        return this._soldier.getName() + " with *sword*";
    }
}
