package soldier;

public interface ISoldier {

    boolean isAlive();
    void parry(int damage);
    int strike();
    int getHealthPoints();
    String getName();
}
