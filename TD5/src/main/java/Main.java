import soldier.*;

public class Main {

    public static void main(String[] args) {
        ISoldier s1 = new Infantryman(100);
        ISoldier s2 = new Horseman(100);

        s2.parry(s1.strike());
        s1 = new SoldierWithShield(s1);
        s2 = new SoldierWithSword(s2);
        s1.parry(s2.strike());
        s2 = new SoldierWithSword(s2);
        s1 = new SoldierWithShield(s1);
        s2.parry(s1.strike());
        s1.parry(s2.strike());

        System.out.println(s1.getHealthPoints());
        System.out.println(s2.getHealthPoints());
 /*       ISoldier attack = s1;
        ISoldier defend = s2;
        while (s1.isAlive() && s2.isAlive()) {
            defend.parry(attack.strike());
            ISoldier tmpSwap = attack;
            attack = defend;
            defend = tmpSwap;
        }
        if (s1.isAlive())
            System.out.println("soldier.Soldier 1  Won");
        else
            System.out.println("soldier.Soldier 2  Won");*/
    }

    void fight(ISoldier s1, ISoldier s2) {
        ISoldier attack = s1;
        ISoldier defend = s2;
        while (s1.isAlive() && s2.isAlive()) {
            defend.parry(attack.strike());
            ISoldier tmpSwap = attack;
            attack = defend;
            defend = tmpSwap;
        }
        if (s1.isAlive())
            System.out.println("soldier.Soldier 1  Won");
        else
            System.out.println("soldier.Soldier 2  Won");
    }

}
