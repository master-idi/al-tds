package builder;

public class HTMLBuilder implements StatementBuilder {

    private StringBuilder _stringBuilder;

    public HTMLBuilder() {
        this._stringBuilder = new StringBuilder();
    }

    @Override
    public void begin() {
        this._stringBuilder
                .append("<!doctype html>")
                .append("<html>")
                .append("<head>")
                .append("<title>Statement</title>")
                .append("</head>")
                .append("<body>");
    }

    @Override
    public void addHeader() {
        this._stringBuilder.append("<header>");
    }

    @Override
    public void closeHeader() {
        this._stringBuilder.append("</header>");
    }

    @Override
    public void addTable() {
        this._stringBuilder.append("<table>");
    }

    @Override
    public void closeTable() {
        this._stringBuilder.append("</table>");
    }

    @Override
    public void addTableHeader() {
        this._stringBuilder.append("<th>");
    }

    @Override
    public void closeTableHeader() {
        this._stringBuilder.append("</th>");
    }

    @Override
    public void addRow() {
        this._stringBuilder.append("<tr>");
    }

    @Override
    public void closeRow() {
        this._stringBuilder.append("</tr>");
    }

    @Override
    public void addColumn() {
        this._stringBuilder.append("<td>");
    }

    @Override
    public void closeColumn() {
        this._stringBuilder.append("</td>");
    }

    @Override
    public void addHeadTypeOne() {
        this._stringBuilder.append("<h1>");
    }

    @Override
    public void closeHeadTypeOne() {
        this._stringBuilder.append("</h1>");
    }

    @Override
    public void addHeadTypeTwo() {
        this._stringBuilder.append("<h2>");
    }

    @Override
    public void closeHeadTypeTwo() {
        this._stringBuilder.append("</h2>");
    }

    @Override
    public void addParagraph() {
        this._stringBuilder.append("<p>");
    }

    @Override
    public void closeParagraph() {
        this._stringBuilder.append("</p>");
    }

    @Override
    public void addFooter() {
        this._stringBuilder.append("<footer>");
    }

    @Override
    public void closeFooter() {
        this._stringBuilder.append("</footer>");
    }

    @Override
    public void addContent(String content) {
        this._stringBuilder.append(content);
    }

    @Override
    public void finish() {
        this._stringBuilder
                .append("<body>")
                .append("<html>");
    }

    public String getStatement() {
        return this._stringBuilder.toString();
    }
}
