package builder;

import javafx.scene.layout.GridPane;

public class UIBuilder implements StatementBuilder {

    private GridPane _grid;

    public UIBuilder() {
        this._grid = new GridPane();
    }

    @Override
    public void begin() {

    }

    @Override
    public void addHeader() {

    }

    @Override
    public void closeHeader() {

    }

    @Override
    public void addTable() {

    }

    @Override
    public void closeTable() {

    }

    @Override
    public void addTableHeader() {

    }

    @Override
    public void closeTableHeader() {

    }

    @Override
    public void addRow() {

    }

    @Override
    public void closeRow() {

    }

    @Override
    public void addColumn() {

    }

    @Override
    public void closeColumn() {

    }

    @Override
    public void addHeadTypeOne() {

    }

    @Override
    public void closeHeadTypeOne() {

    }

    @Override
    public void addHeadTypeTwo() {

    }

    @Override
    public void closeHeadTypeTwo() {

    }

    @Override
    public void addParagraph() {

    }

    @Override
    public void closeParagraph() {

    }

    @Override
    public void addFooter() {

    }

    @Override
    public void closeFooter() {

    }

    @Override
    public void addContent(String content) {

    }

    @Override
    public void finish() {

    }

    public GridPane getStatement() {
        return this._grid;
    }
}
