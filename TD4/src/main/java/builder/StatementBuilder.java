package builder;

public interface StatementBuilder {

    void begin();

    void addHeader();
    void closeHeader();

    void addTable();
    void closeTable();

    void addTableHeader();
    void closeTableHeader();

    void addRow();
    void closeRow();

    void addColumn();
    void closeColumn();

    void addHeadTypeOne();
    void closeHeadTypeOne();

    void addHeadTypeTwo();
    void closeHeadTypeTwo();

    void addParagraph();
    void closeParagraph();

    void addFooter();
    void closeFooter();

    void addContent(String content);

    void finish();
}
