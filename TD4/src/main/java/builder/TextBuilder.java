package builder;

public class TextBuilder implements StatementBuilder {

    private StringBuilder _stringBuilder;

    public TextBuilder() {
        this._stringBuilder = new StringBuilder();
    }

    @Override
    public void begin() {

    }

    @Override
    public void addHeader() {

    }

    @Override
    public void closeHeader() {
        this._stringBuilder.append('\n');
    }

    @Override
    public void addTable() {

    }

    @Override
    public void closeTable() {

    }

    @Override
    public void addTableHeader() {

    }

    @Override
    public void closeTableHeader() {

    }

    @Override
    public void addRow() {

    }

    @Override
    public void closeRow() {
        this._stringBuilder.append('\n');
    }

    @Override
    public void addColumn() {
        this._stringBuilder.append('\t');
    }

    @Override
    public void closeColumn() {

    }

    @Override
    public void addHeadTypeOne() {

    }

    @Override
    public void closeHeadTypeOne() {

    }

    @Override
    public void addHeadTypeTwo() {

    }

    @Override
    public void closeHeadTypeTwo() {

    }

    @Override
    public void addParagraph() {
        this._stringBuilder.append('\n');
    }

    @Override
    public void closeParagraph() {

    }

    @Override
    public void addFooter() {

    }

    @Override
    public void closeFooter() {

    }

    @Override
    public void addContent(String content) {
        this._stringBuilder.append(content);
    }

    @Override
    public void finish() {
    }

    public String getStatement() {
        return this._stringBuilder.toString();
    }
}
