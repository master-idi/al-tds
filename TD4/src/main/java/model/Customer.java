package model;

import builder.StatementBuilder;

import java.util.ArrayList;

public class Customer {
    private String _name;
    private ArrayList<Rental> _rentals = new ArrayList<>();

    public Customer(String name) {
        _name = name;
    }

    public void addRental(Rental rental) {
        _rentals.add(rental);
    }

    public String getName() {
        return _name;
    }

    public void statement(StatementBuilder builder) {
        builder.begin();

        builder.addHeader();
        builder.addHeadTypeOne();
        builder.addContent(this._name + "'s statement");
        builder.closeHeadTypeOne();
        builder.closeHeader();

        builder.addTable();
        builder.addRow();
        builder.addTableHeader();
        builder.addContent("Movie title");
        builder.closeTableHeader();

		builder.addTableHeader();
		builder.addContent("Day(s) rented");
		builder.closeTableHeader();

        builder.addTableHeader();
        builder.addContent("Amount");
        builder.closeTableHeader();
        builder.closeRow();

        for (Rental rental : this._rentals) {
			builder.addRow();
			builder.addColumn();
			builder.addContent(rental.getMovie().getTitle());
			builder.closeColumn();

			builder.addColumn();
			builder.addContent(String.valueOf(rental.getDaysRented()));
			builder.closeColumn();

			builder.addColumn();
			builder.addContent(String.valueOf(rental.getAmount()));
			builder.closeColumn();
			builder.closeRow();
        }
        builder.addRow();

        builder.addColumn();
        builder.addContent("TOTAL :");
        builder.closeColumn();

		builder.addColumn();
		builder.addContent(String.valueOf(this.getTotalRentedDays()));
		builder.closeColumn();

		builder.addColumn();
		builder.addContent(String.valueOf(this.getTotalAmount()));
		builder.closeColumn();

        builder.closeRow();

        builder.closeTable();

		builder.addParagraph();
		builder.addContent("You have " + this.getRenterPoints() + " renter points !");

		builder.finish();
    }


    double getTotalAmount() {
        double res = 0;
        for (Rental each : _rentals) {
            res += each.getAmount();
        }
        return res;
    }

	int getTotalRentedDays() {
		int res = 0;
		for (Rental each : _rentals) {
			res += each.getDaysRented();
		}
		return res;
	}

    int getRenterPoints() {
        int res = 0;
        for (Rental each : _rentals) {
            res += each.getRenterPoints();
        }
        return res;
    }
}
