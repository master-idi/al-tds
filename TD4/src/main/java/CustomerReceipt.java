import builder.HTMLBuilder;
import builder.TextBuilder;
import builder.UIBuilder;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import model.*;

public class CustomerReceipt extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {

        NewReleasePricing newReleasePriceCode = new NewReleasePricing();
        StandardPricing childrenPriceCode = new StandardPricing(1.5, 1.5, 3);
        StandardPricing regularPriceCode = new StandardPricing(2, 1.5, 2);

        Movie rogueOne = new Movie("Rogue One", newReleasePriceCode);
        Movie frozen = new Movie("Reine des neiges", childrenPriceCode);
        Movie revengeOfTheSith = new Movie("Star Wars III", regularPriceCode);

        Customer customer = new Customer("John Doe");

        customer.addRental(new Rental(rogueOne, 5));
        customer.addRental(new Rental(frozen, 7));
        customer.addRental(new Rental(revengeOfTheSith, 4));

        // SETUP BUILDERS
        TextBuilder txt = new TextBuilder();
        customer.statement(txt);

        // Build the HTML statement
        HTMLBuilder html = new HTMLBuilder();
        customer.statement(html);

        // Build the JavaFX statement
        UIBuilder ui = new UIBuilder();
        customer.statement(ui);


        /// Statement display ///
        // We will show all our statements in a JavaFX grid :
        GridPane root = new GridPane();

        // Display the text statement
        root.add(new Text("Plain text"), 0, 0);
        root.add(new Text(txt.getStatement()), 0, 1);

        // Also put it on the console :
        System.out.println(txt.getStatement());


        // Display the HTML statement in a small webview
        root.add(new Text("HTML"), 1, 0);
        WebView webView = new WebView();
        webView.getEngine().loadContent(html.getStatement());
        webView.setVisible(true);
        webView.setMaxWidth(400);
        webView.setMaxHeight(400);
        root.add(webView, 1, 1);

        // Also put it on the console :
        System.out.println(html.getStatement());


        // Display the JavaFX statement
        root.add(new Text("JavaFX"), 2, 0);
        root.add(ui.getStatement(), 2, 1);


        /// JavaFX window set-up ///
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(new Scene(root, 0, 0));
        primaryStage.setMinHeight(500);
        primaryStage.setMinWidth(1000);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
