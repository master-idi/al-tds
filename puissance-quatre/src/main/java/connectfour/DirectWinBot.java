package connectfour;

import connectfour.core.player.BotPlayer;
import connectfour.core.player.DirectWinBotPlayer;
import connectfour.core.player.OpponentBotPlayer;

/**
 * Class that represents a bot that plays to win at every opportunity.
 *
 * @deprecated
 * This class is no longer acceptable to instantiate a direct win bot player.
 * <p>Use {@link connectfour.core.player.DirectWinBotPlayer} instead.</p>
 */
@Deprecated
public class DirectWinBot extends DirectWinBotPlayer implements P4Player {

    public DirectWinBot(Puissance4 p) {
        super(new OpponentBotPlayer(new BotPlayer()));
    }
}
