package connectfour;

import connectfour.core.ConnectFourImpl;
import connectfour.core.GridBuilder;
import connectfour.core.IConnectFour;
import connectfour.core.player.IPlayer;

/**
 * Interface to implement a connect four game.
 *
 * @deprecated
 * This interface is no longer acceptable to implement power four game.
 * <p>User {@link IConnectFour} instead.</p>
 */
@Deprecated
public interface Puissance4 {
    int WIDTH = ConnectFourImpl.WIDTH;
    int HEIGHT = ConnectFourImpl.HEIGHT;

    /**
     * Initialize all data required to play a game (player one/two, array of tokens, available places and
     * game state boolean)
     *
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#init(IPlayer, IPlayer)} instead.</p>
     */
    @Deprecated
    void init(P4Player p1, P4Player p2);

    /**
     * Get the instance of the player one.
     *
     * @return return an instance of {@link P4Player}
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#getPlayerOne()} instead.</p>
     */
    @Deprecated
    P4Player player1();

    /**
     * Get the instance of the player two.
     *
     * @return return an instance of {@link P4Player}
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#getPlayerTwo()} instead.</p>
     */
    @Deprecated
    P4Player player2();

    /**
     * Get the instance of the current player.
     *
     * @return return an instance of {@link P4Player}
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#getCurrentPlayer()} instead.</p>
     */
    @Deprecated
    P4Player currentPlayer();

    /**
     * Check if the game is ended.
     *
     * @return <code>true</code> if game is ended, <code>false</code> else/
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#isFinished()} instead.</p>
     */
    @Deprecated
    boolean end();

    /**
     * If the game is not finished and the given <code>col</code> parameter is valid, place it to the grid
     * and check if the current player win.
     *
     * @param col the column number where to place token of the current player.
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#placeToken(int)} (int)} instead.</p>
     */
    @Deprecated
    void play(int col);

    /**
     * Build the grid that represents the game to display.
     *
     * @return return a <code>String</code> that contains the grid to display.
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link GridBuilder#createNewGrid()} instead.</p>
     */
    @Deprecated
    String toString();

    /**
     * Check if cell at the given <code>col</code> parameter is free.
     *
     * @param col the column number of the cell to check if it's free or not.
     * @return return <code>true</code> if cell is free, <code>false</code> else.
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#isFree(int)} instead.</p>
     */
    @Deprecated
    boolean isFree(int col);

    /**
     * Check if the <code>player</code> can win the game with the given <code>col</code> parameter.
     *
     * @param col the column number of the cell to check.
     * @param player the player who place the token.
     * @return <code>true</code> if the <code>player</code> win the game with this <code>col</code>,
     * <code>false</code> else.
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#canPlayerWin(int, IPlayer)} instead.</p>
     */
    @Deprecated
    boolean checkWin(int col, P4Player player);

    /**
     * Return the current 2 dimensions array that represents the connect four game.
     *
     * @return Return a 2 dimensions array of type {@link P4Player}
     * @deprecated
     * This method is no longer acceptable to init connect four game.
     * <p>Use {@link IConnectFour#getPlayersTokens()} instead.</p>
     */
    @Deprecated
    P4Player[][] getTab();
}
