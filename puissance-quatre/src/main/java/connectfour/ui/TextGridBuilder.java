package connectfour.ui;

import connectfour.core.GridBuilder;

/**
 * Class that represents an implementation of the grid builder through string
 */
public class TextGridBuilder implements GridBuilder {

    private StringBuilder _gridBuilder;

    @Override
    public void createNewGrid() {
        this._gridBuilder = new StringBuilder();
    }

    @Override
    public void header() {
        this._gridBuilder.append("***************\n");
    }

    @Override
    public void footer() {
        this._gridBuilder.append("***************\n");
    }

    @Override
    public void addCell(String player) {
        this._gridBuilder
                .append(player)
                .append("|");
    }

    @Override
    public void beginRow() {
        this._gridBuilder.append("|");
    }

    @Override
    public void endRow() {
        this._gridBuilder.append("\n");
    }

    public String getGrid() {
        return this._gridBuilder.toString();
    }
}
