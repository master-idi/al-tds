package connectfour.ui;

import connectfour.core.ResultWinBuilder;
import connectfour.core.player.IPlayer;

/**
 * Class that represents an implementation of the game result builder through string
 */
public class TextResultWinBuilder implements ResultWinBuilder {

    @Override
    public void printResultWin(IPlayer player) {
        System.out.println("player " + player + " win");
    }
}
