package connectfour.ui;

import connectfour.core.InfoBuilder;
import connectfour.core.player.IPlayer;

/**
 * Class that represents an implementation of the info builder through string
 */
public class TextInfoBuilder implements InfoBuilder {
    private StringBuilder _infoBuilder;

    @Override
    public void createNewInfo() {
        this._infoBuilder = new StringBuilder();
    }

    @Override
    public void info(IPlayer player) {
        this._infoBuilder
                .append("Player ")
                .append(player)
                .append(" turn");
    }

    public String getInfo() {
        return this._infoBuilder.toString();
    }
}
