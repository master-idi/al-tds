package connectfour.core;

import connectfour.core.player.IPlayer;

/**
 * Interface to implement a info builder
 */
public interface InfoBuilder {

    /**
     * Instantiate a new info according to the concrete implementation.
     */
    void createNewInfo();

    /**
     * Displays a message explaining that the given <code>player</code> will play in this round.
     *
     * @param player represents an instance of a player that we want to show into the info message
     */
    void info(IPlayer player);
}
