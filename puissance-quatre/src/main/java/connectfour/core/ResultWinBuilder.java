package connectfour.core;

import connectfour.core.player.IPlayer;

/**
 * Interface to implement a result win builder
 */
public interface ResultWinBuilder {

    /**
     * Display the given winner <code>player</code>.
     *
     * @param player an instance of a player which has won.
     */
    void printResultWin(IPlayer player);
}
