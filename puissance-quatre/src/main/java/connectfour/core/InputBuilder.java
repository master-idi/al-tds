package connectfour.core;

import java.io.IOException;

/**
 * Interface to implement a input builder
 */
public interface InputBuilder {

    /**
     * Displays the given <code>message</code> before player interaction.
     *
     * @param message the message to display.
     */
    void printInfo(String message);

    /**
     * Displays the given <code>message</code> error after player interaction.
     *
     * @param message error message to display.
     */
    void printError(String message);

    /**
     * Allow player to enter a column number of a grid cell.
     *
     * @return return a string contains the player input.
     * @throws IOException exception thrown if input error happened.
     */
    String readInput() throws IOException;
}
