package connectfour.core.player;

import connectfour.core.IConnectFour;
import connectfour.core.ConnectFourImpl;
import connectfour.core.InputBuilder;

import java.io.IOException;

/**
 * Class that represents a human player.
 */
public class HumanPlayer extends Player {
    private final IConnectFour _connectFour;
    private final InputBuilder _inputBuilder;

    public HumanPlayer(InputBuilder inputBuilder) {
        this._inputBuilder = inputBuilder;
        this._connectFour = ConnectFourImpl.getInstance();
    }

    @Override
    public int play() {
        int col = -1;
        while (!this._connectFour.isFree(col)) {
            this._inputBuilder.printInfo("Enter column :");
            try {
                String response = this._inputBuilder.readInput();
                try {
                    col = Integer.parseInt(response);
                } catch (Exception e) {
                    this._inputBuilder.printError("Bad width value");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return col;
    }
}
