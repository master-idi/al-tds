package connectfour.core.player;

import connectfour.core.IConnectFour;
import connectfour.core.ConnectFourImpl;

/**
 * Class that represents a bot that plays in such a way as to prevent the opposing player from winning.
 */
public class OpponentBotPlayer extends PlayerDecorator {
    private final IConnectFour _connectFour;
    private final IPlayer _player;

    public OpponentBotPlayer(IPlayer player) {
        super(player);
        this._connectFour = ConnectFourImpl.getInstance();
        this._player = player;
    }

    @Override
    public int play() {
        IPlayer opponent;
        if (this._connectFour.getPlayerOne() != this)
            opponent = this._connectFour.getPlayerOne();
        else
            opponent = this._connectFour.getPlayerTwo();
        for (int col = 0; col < IConnectFour.WIDTH; ++col) {
            if (this._connectFour.canPlayerWin(col, opponent))
                return col;
        }
        return this._player.play();
    }
}
