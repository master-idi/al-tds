package connectfour.core.player;

import connectfour.core.IConnectFour;
import connectfour.core.ConnectFourImpl;

import java.util.Random;

/**
 * Class that represents a bot that plays randomly on the board game.
 */
public class BotPlayer extends Player {
    private final IConnectFour _connectFour;

    public BotPlayer() {
        this._connectFour = ConnectFourImpl.getInstance();
    }

    @Override
    public int play() {
        Random r = new Random();
        int res = r.nextInt(IConnectFour.WIDTH);
        while (!this._connectFour.isFree(res))
            res = r.nextInt(IConnectFour.WIDTH);
        return res;
    }
}
