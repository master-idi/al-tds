package connectfour.core.player;

/**
 * Abstraction of player.
 * Should contain common properties (such scores, etc...) of the different types of players.
 */
public abstract class Player implements IPlayer {

}
