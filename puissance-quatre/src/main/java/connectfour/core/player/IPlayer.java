package connectfour.core.player;

/**
 * Interface to implement a connect four player
 */
public interface IPlayer {

    /**
     * Implements the way different types of players play.
     *
     * @return return the column number of the cell to place on the board game.
     */
    int play();
}
