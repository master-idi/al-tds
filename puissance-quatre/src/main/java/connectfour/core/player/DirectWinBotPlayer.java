package connectfour.core.player;

import connectfour.core.IConnectFour;
import connectfour.core.ConnectFourImpl;

/**
 * Class that represents a bot that plays to win at every opportunity.
 */
public class DirectWinBotPlayer extends PlayerDecorator {
    private final IConnectFour _connectFour;
    private final IPlayer _player;

    public DirectWinBotPlayer(IPlayer player) {
        super(player);
        this._player = player;
        this._connectFour = ConnectFourImpl.getInstance();
    }

    @Override
    public int play() {
        for (int i = 0; i < IConnectFour.WIDTH; ++i) {
            if (this._connectFour.canPlayerWin(i, this))
                return i;
        }
        return this._player.play();
    }
}
