package connectfour.core.player;


/**
 * Represents an abstraction of a player decorator.
 */
public abstract class PlayerDecorator implements IPlayer {

    private final IPlayer _player;

    public PlayerDecorator(IPlayer player) {
        this._player = player;
    }
}
