package connectfour.core;

import connectfour.core.player.IPlayer;

/**
 * Interface to implement a player factory builder
 */
public interface IPlayerFactory {

    /**
     * Get an instance of a human player.
     *
     * @param inputBuilder an instance of a concrete implementation of the {@link InputBuilder}
     * @return return an instance of a {@link connectfour.core.player.HumanPlayer}.
     */
    IPlayer getHumanPlayer(InputBuilder inputBuilder);

    /**
     * Get an instance of an opponent player.
     *
     * @return return an instance of a {@link connectfour.core.player.OpponentBotPlayer}
     */
    IPlayer getOpponentBotPlayer();

    /**
     * Get an instance of a direct win bot player with an opponent bot player decoration.
     *
     * @return an instance of a {@link connectfour.core.player.DirectWinBotPlayer} with an {@link connectfour.core.player.OpponentBotPlayer} decoration.
     */
    IPlayer getDirectWinOpponentBotPlayer();
}
