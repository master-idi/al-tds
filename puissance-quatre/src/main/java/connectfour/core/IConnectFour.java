package connectfour.core;

import connectfour.P4Player;
import connectfour.core.player.IPlayer;

/**
 * Interface to implement a connect four game
 */
public interface IConnectFour {
    int WIDTH = 7;
    int HEIGHT = 7;
    int WINNING_THRESHOLD = 4;

    /**
     * Initialize all data required to play a game (player one/two, array of tokens, available places and
     * game state boolean)
     *
     * @param playerOne an instance of {@link IPlayer} to initialize as player one.
     * @param playerTwo an instance of {@link IPlayer} to initialize as player two.
     */
    void init(IPlayer playerOne, IPlayer playerTwo);

    /**
     * Initialize all data required to play a game (player one/two, array of tokens, available places and
     * game state boolean)
     *
     * @param playerOne an instance of {@link IPlayer} to initialize as player one.
     * @param playerTwo an instance of {@link IPlayer} to initialize as player two.
     */
    void init(IPlayer playerOne, IPlayer playerTwo, ResultWinBuilder resultWinBuilder);

    /**
     * Get the instance of the player one.
     *
     * @return return an instance of {@link IPlayer}
     */
    IPlayer getPlayerOne();

    /**
     * Get the instance of the player two.
     *
     * @return return an instance of {@link IPlayer}
     */
    IPlayer getPlayerTwo();

    /**
     * Get the instance of the current player.
     *
     * @return return an instance of {@link IPlayer}
     */
    IPlayer getCurrentPlayer();

    /**
     * Check if the game is ended.
     *
     * @return <code>true</code> if game is ended, <code>false</code> else/
     */
    boolean isFinished();

    /**
     * If the game is not finished and the given <code>col</code> parameter is valid, place it to the grid
     * and check if the current player win.
     *
     * @param col the column number where to place token of the current player.
     */
    void placeToken(int col);

    /**
     * Check if cell at the given <code>col</code> parameter is free.
     *
     * @param col the column number of the cell to check if it's free or not.
     * @return return <code>true</code> if cell is free, <code>false</code> else.
     */
    boolean isFree(int col);

    /**
     * Check if the <code>player</code> can win the game with the given <code>col</code> parameter.
     *
     * @param col the column number of the cell to check.
     * @param player the player who place the token.
     * @return <code>true</code> if the <code>player</code> win the game with this <code>col</code>,
     * <code>false</code> else.
     */
    boolean canPlayerWin(int col, IPlayer player);

    /**
     * Return the current 2 dimensions array that represents the connect four game.
     *
     * @return Return a 2 dimensions array of type {@link P4Player}
     */
    IPlayer[][] getPlayersTokens();

    /**
     * Build the connect four board game from the given builder parameter.
     *
     * @param gridBuilder instance of {@link GridBuilder} to use.
     */
    void buildGrid(GridBuilder gridBuilder);

    /**
     * Build the connect four info from the given builder parameter.
     *
     * @param infoBuilder instance of {@link InfoBuilder} to use.
     */
    void buildInfo(InfoBuilder infoBuilder);
}
