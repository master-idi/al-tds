package connectfour.core;

/**
 * Represents the connect four abstract factory.
 */
public abstract class ConnectFourFactory {

    public static PlayerFactory getPlayerFactory() {
        return new PlayerFactory();
    }
}
