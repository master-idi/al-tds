package connectfour.core;

import connectfour.core.player.IPlayer;

/**
 * Class that implements a connect four game.
 */
public class ConnectFourImpl implements IConnectFour {

    private static final ConnectFourImpl _instance = new ConnectFourImpl();

    private ResultWinBuilder _resultWinBuilder;

    private ConnectFourImpl() {
    }

    public static ConnectFourImpl getInstance() {
        return _instance;
    }

    private IPlayer[][] _playersTokens;
    private boolean _isFinished;
    private int _availablePlaces;
    private IPlayer _currentPlayer, _playerOne, _playerTwo;

    @Override
    public void init(IPlayer playerOne, IPlayer playerTwo) {
        this._playerOne = playerOne;
        this._playerTwo = playerTwo;
        this._currentPlayer = this._playerOne;
        this._playersTokens = new IPlayer[WIDTH][HEIGHT];
        this.initPlayersTokens();
        this._isFinished = false;
        this._availablePlaces = WIDTH * HEIGHT;
    }

    @Override
    public void init(IPlayer playerOne, IPlayer playerTwo, ResultWinBuilder resultWinBuilder) {
        this._resultWinBuilder = resultWinBuilder;
        this.init(playerOne, playerTwo);
    }

    @Override
    public void buildGrid(GridBuilder gridBuilder) {
        gridBuilder.createNewGrid();
        gridBuilder.header();
        for (int i = WIDTH - 1; i >= 0; --i) {
            gridBuilder.beginRow();
            for (int j = 0; j < HEIGHT; ++j) {
                if (this._playersTokens[i][j] == this._playerOne)
                    gridBuilder.addCell("X");
                if (this._playersTokens[i][j] == null)
                    gridBuilder.addCell(" ");
                if (this._playersTokens[i][j] == this._playerTwo)
                    gridBuilder.addCell("O");
            }
            gridBuilder.endRow();
        }
        gridBuilder.footer();
    }

    @Override
    public void buildInfo(InfoBuilder infoBuilder) {
        infoBuilder.createNewInfo();
        infoBuilder.info(this.getCurrentPlayer());
    }

    @Override
    public boolean isFree(int col) {
        if (this._availablePlaces <= 0 || col < 0 || col >= WIDTH)
            return false;
        return this.getFirstAvailableRow(col) < HEIGHT;
    }

    @Override
    public boolean canPlayerWin(int col, IPlayer player) {
        if (!this.isFree(col)) return false;
        return this.isWinningMove(this.getFirstAvailableRow(col), col, player);
    }

    @Override
    public IPlayer[][] getPlayersTokens() {
        return this._playersTokens;
    }

    @Override
    public IPlayer getPlayerOne() {
        return this._playerOne;
    }

    @Override
    public IPlayer getPlayerTwo() {
        return this._playerTwo;
    }

    @Override
    public IPlayer getCurrentPlayer() {
        return this._currentPlayer;
    }

    @Override
    public boolean isFinished() {
        return this._isFinished;
    }

    @Override
    public void placeToken(int col) {
        if (this.isFinished()) return;
        --this._availablePlaces;
        int row = this.getFirstAvailableRow(col);
        if (row >= HEIGHT) {
            // we should not access this statement in normal use but if this is the case we throw an exception
            throw new IndexOutOfBoundsException("col is full");
        }
        this._playersTokens[row][col] = this._currentPlayer;
        if (this.isWinningMove(row, col, this._currentPlayer)) {
            this.finish();
            return;
        }
        this._currentPlayer = switchPlayer();
    }

    private void finish() {
        if (this._resultWinBuilder != null) {
            this._resultWinBuilder.printResultWin(this._currentPlayer);
        }
        this._isFinished = true;
    }

    private int getFirstAvailableRow(int col) {
        int row = 0;
        while (row < HEIGHT && this._playersTokens[row][col] != null)
            ++row;
        return row;
    }

    private IPlayer switchPlayer() {
        if (this._currentPlayer == this._playerOne)
            return this._playerTwo;
        else
            return this._playerOne;
    }

    private void initPlayersTokens() {
        for (int i = 0; i < WIDTH; ++i)
            for (int j = 0; j < HEIGHT; ++j)
                this._playersTokens[i][j] = null;
    }

    /**
     * Check if the next move of the given <code>player</code> is a winning move or not.
     *
     * @param row    row number of the cell to check.
     * @param col    col number of the cell to check.
     * @param player instance of player who plays.
     * @return return <code>true</code> if the next move of the player is a winning move, <code>false</code> else.
     */
    private boolean isWinningMove(int row, int col, IPlayer player) {
        return this.countRowPlayerTokens(row, col, player) >= WINNING_THRESHOLD
                || this.countColPlayerTokens(row, col, player) >= WINNING_THRESHOLD
                || this.countDiagonalRightPlayerTokens(row, col, player) >= WINNING_THRESHOLD
                || this.countDiagonalLeftPlayerTokens(row, col, player) >= WINNING_THRESHOLD;
    }

    private int countRowPlayerTokens(int row, int col, IPlayer player) {
        int counter = 1;
        for (int x = row + 1; x < WIDTH && this._playersTokens[x][col] == player; ++x) ++counter;
        for (int x = row - 1; x >= 0 && this._playersTokens[x][col] == player; --x) ++counter;
        return counter;
    }

    private int countColPlayerTokens(int row, int col, IPlayer player) {
        int counter = 1;
        for (int x = col + 1; x < WIDTH && this._playersTokens[row][x] == player; ++x) ++counter;
        for (int x = col - 1; x >= 0 && this._playersTokens[row][x] == player; --x) ++counter;
        return counter;
    }

    private int countDiagonalRightPlayerTokens(int row, int col, IPlayer player) {
        int counter = 1;
        for (int x = row + 1, y = col + 1; x < WIDTH && y < HEIGHT && this._playersTokens[x][y] == player; ++x, ++y)
            ++counter;
        for (int x = row - 1, y = col - 1; x >= 0 && y >= 0 && this._playersTokens[x][y] == player; --x, --y) ++counter;
        return counter;
    }

    private int countDiagonalLeftPlayerTokens(int row, int col, IPlayer player) {
        int counter = 1;
        for (int x = row + 1, y = col - 1; x < WIDTH && y >= 0 && this._playersTokens[x][y] == player; ++x, --y)
            ++counter;
        for (int x = row - 1, y = col + 1; x >= 0 && y < HEIGHT && this._playersTokens[x][y] == player; --x, ++y)
            ++counter;
        return counter;
    }
}
