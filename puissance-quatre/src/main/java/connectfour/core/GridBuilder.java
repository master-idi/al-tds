package connectfour.core;

/**
 * Interface to implement a grid builder
 */
public interface GridBuilder {


    /**
     * Instantiate a new grid according to the concrete implementation.
     */
    void createNewGrid();

    /**
     * Append the instantiated grid with a header.
     */
    void header();

    /**
     * Append the instantiated grid with a footer.
     */
    void footer();

    /**
     * Append a new cell to the grid filled with the given <code>player</code> character.
     *
     * @param player a character that represents a player.
     */
    void addCell(String player);

    /**
     * Append the start of a new row to the grid.
     */
    void beginRow();

    /**
     * Append the end of a row to the grid.
     */
    void endRow();
}
