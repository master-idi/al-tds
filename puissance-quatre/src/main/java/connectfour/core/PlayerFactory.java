package connectfour.core;

import connectfour.core.player.*;

/**
 * Class that represents an implementation of player factory.
 */
class PlayerFactory implements IPlayerFactory {
    @Override
    public IPlayer getHumanPlayer(InputBuilder inputBuilder) {
        return new HumanPlayer(inputBuilder);
    }

    @Override
    public IPlayer getOpponentBotPlayer() {
        return new OpponentBotPlayer(new BotPlayer());
    }

    @Override
    public IPlayer getDirectWinOpponentBotPlayer() {
        return new DirectWinBotPlayer(new OpponentBotPlayer(new BotPlayer()));
    }
}
