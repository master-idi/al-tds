package connectfour;

import connectfour.core.player.BotPlayer;
import connectfour.core.player.OpponentBotPlayer;

/**
 * Class that represents a bot that plays in such a way as to prevent the opposing player from winning.
 *
 * @deprecated
 * This class is no longer acceptable to instantiate an opponent bot player.
 * <p>Use {@link connectfour.core.player.OpponentBotPlayer} instead.</p>
 */
@Deprecated
public class OpponentBot extends OpponentBotPlayer implements P4Player {

    public OpponentBot(Puissance4 p) {
        super(new BotPlayer());
    }
}
