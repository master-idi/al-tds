package connectfour;

import connectfour.core.player.BotPlayer;

/**
 * Class that represents a bot that plays randomly on the board game.
 *
 * @deprecated
 * This interface is no longer acceptable to instantiate a bot player.
 * <p>Use {@link connectfour.core.player.BotPlayer} instead.</p>
 */
@Deprecated
public class RandomBot extends BotPlayer {

    public RandomBot(Puissance4 p) {
        super();
    }
}

