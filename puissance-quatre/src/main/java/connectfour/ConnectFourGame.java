package connectfour;

import connectfour.core.*;
import connectfour.core.player.IPlayer;
import connectfour.ui.TextResultWinBuilder;
import connectfour.ui.TextGridBuilder;
import connectfour.ui.TextInfoBuilder;
import connectfour.ui.TextInputBuilder;

/**
 * Class that represents the new main implementation of a connect four game.
 */
public class ConnectFourGame {

    public static void run(IConnectFour p) {
        TextGridBuilder txtGrid = new TextGridBuilder();
        TextInfoBuilder txtInfo = new TextInfoBuilder();
        while (!p.isFinished()) {
            p.buildGrid(txtGrid);
            System.out.println(txtGrid.getGrid());
            p.buildInfo(txtInfo);
            System.out.println(txtInfo.getInfo());
            p.placeToken(p.getCurrentPlayer().play());
        }
        p.buildGrid(txtGrid);
        System.out.println(txtGrid.getGrid());
    }

    public static void main(String argv[]) {
        IPlayerFactory factory = ConnectFourFactory.getPlayerFactory();
        IPlayer humanPlayer = factory.getHumanPlayer(new TextInputBuilder());
        IPlayer botPlayer = factory.getDirectWinOpponentBotPlayer();
        IConnectFour p = ConnectFourImpl.getInstance();

        p.init(humanPlayer, botPlayer, new TextResultWinBuilder());
        run(p);
    }

}
