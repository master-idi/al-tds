package connectfour;

import connectfour.core.player.IPlayer;

/**
 * Interface that represents a connect four player.
 *
 * @deprecated
 * This interface is no longer acceptable to implement a player.
 * <p>Use {@link IPlayer} instead.</p>
 */
@Deprecated
public interface P4Player extends IPlayer {
}
