package connectfour;

import connectfour.core.ConnectFourImpl;
import connectfour.ui.TextResultWinBuilder;

/**
 * Class that represents an implementation of the connect four game.
 *
 * @deprecated
 * This interface is no longer acceptable to instantiate a connect four game.
 * <p>Use {@link ConnectFourImpl} instead.</p>
 */
@Deprecated
public class Puissance4Impl implements Puissance4 {
    private final ConnectFourImpl _connectFour;

    public Puissance4Impl() {
        this._connectFour = ConnectFourImpl.getInstance();
    }

    @Override
    public P4Player currentPlayer() {
        return (P4Player) this._connectFour.getCurrentPlayer();
    }

    @Override
    public P4Player player1() {
        return (P4Player) this._connectFour.getPlayerOne();
    }

    @Override
    public P4Player player2() {
        return (P4Player) this._connectFour.getPlayerTwo();
    }

    @Override
    public void init(P4Player p1, P4Player p2) {
        this._connectFour.init(p1, p2, new TextResultWinBuilder());
    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append("***************\n");
        for (int i = WIDTH - 1; i >= 0; --i) {
            str.append("|");
            for (int j = 0; j < HEIGHT; ++j) {
                if (this._connectFour.getPlayersTokens()[i][j] == this.player1())
                    str.append("X");
                if (this._connectFour.getPlayersTokens()[i][j] == null)
                    str.append(" ");
                if (this._connectFour.getPlayersTokens()[i][j] == this.player2())
                    str.append("O");
                str.append("|");
            }
            str.append("\n");
        }
        str.append("***************\n");
        return str.toString();
    }

    @Override
    public boolean end() {
        return this._connectFour.isFinished();
    }

    @Override
    public boolean isFree(int col) {
        return this._connectFour.isFree(col);
    }

    @Override
    public void play(int col) {
        this._connectFour.placeToken(col);
    }

    @Override
    public boolean checkWin(int col, P4Player player) {
        return this._connectFour.canPlayerWin(col, player);
    }

    @Override
    public P4Player[][] getTab() {
        P4Player[][] tmp = new P4Player[WIDTH][HEIGHT];

        for (int i = 0; i < WIDTH; ++i)
            for (int j = 0; j < HEIGHT; ++j)
                tmp[i][j] = (P4Player) this._connectFour.getPlayersTokens()[i][j];
        return tmp;
    }
}
