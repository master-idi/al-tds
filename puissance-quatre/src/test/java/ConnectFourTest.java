import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import connectfour.*;

import static org.junit.jupiter.api.Assertions.*;


public class ConnectFourTest {

    Puissance4 board;
    P4Player bot1;
    P4Player bot2;
    @BeforeEach
    void setUp() {
        this.board = new Puissance4Impl();
        this.bot1 = new HumanBot(this.board);
        this.bot2 = new HumanBot(this.board);
        this.board.init(this.bot1, this.bot2);
    }

    @AfterEach
    void tearDown() {
        this.board = new Puissance4Impl();
        this.bot1 = new HumanBot(this.board);
        this.bot2 = new HumanBot(this.board);
        this.board.init(this.bot1, this.bot2);
    }

    @Test
    void checkIfGameStarts() {
        assertFalse(this.board.end());
    }

    @Test
    void checkIfPiecesAreAtRightPosition() {
        this.board.play(2);
        this.board.play(5);
        this.board.play(1);

        assertNotNull(this.board.getTab()[0][2]);
        assertNotNull(this.board.getTab()[0][5]);
        assertNotNull(this.board.getTab()[0][1]);

        assertNull(this.board.getTab()[1][2]);
        this.board.play(2);
        assertNotNull(this.board.getTab()[1][2]);

        assertNull(this.board.getTab()[0][3]);
        assertNull(this.board.getTab()[4][3]);
    }

    /*
    ****************
    | | | | | | | |
    | | | | | | | |
    | | | | | | | |
    | | | | | | |O|
    | |X| | | | |O|
    | |X| | | | |O|
    | |X| | | | |O|
    ****************
    */
    @Test
    void checkColVictory() {
        this.board.play(6);
        this.board.play(1);
        this.board.play(6);
        this.board.play(1);
        this.board.play(6);
        this.board.play(1);

        assertTrue(this.board.checkWin(6, this.bot1));
    }

    /*
    ****************
    | | | | | | | |
    | | | | | | | |
    | | | | | | | |
    | | | | | | | |
    | |X|X|X| | | |
    |X|O|O|O| | | |
    |O|X|O|X|O|X|O|
    ****************
    */
    @Test
    void checkRowVictory() {
        this.board.play(0);
        this.board.play(1);
        this.board.play(2);
        this.board.play(3);
        this.board.play(4);
        this.board.play(5);
        this.board.play(6);

        this.board.play(0);
        this.board.play(1);
        this.board.play(1);
        this.board.play(2);
        this.board.play(2);
        this.board.play(3);
        this.board.play(3);

        assertTrue(this.board.checkWin(4, this.bot1));
    }

    /*
    ****************
    | | | | | | | |
    | | | | | | | |
    | | | | | | | |
    | | | |O| | | |
    | | | |X|O| | |
    | | | |X|X|O| |
    |O| | |X|X|X|O|
    ****************
    */
    @Test
    void checkDiagonalLeftVictory() {
        this.board.play(6);
        this.board.play(5);
        this.board.play(5);
        this.board.play(4);
        this.board.play(0);
        this.board.play(4);
        this.board.play(4);
        this.board.play(3);
        this.board.play(3);
        this.board.play(3);

        assertTrue(this.board.checkWin(3, this.bot1));
    }

    /*
    ****************
    | | | | | | | |
    | | | | | | | |
    | | | | | | | |
    | | | |O| | | |
    | | |0|X| | | |
    | |0|X|X| | | |
    |O|X|X|X| | |O|
    ****************
    */
    @Test
    void checkDiagonalRightVictory() {
        this.board.play(0);
        this.board.play(1);
        this.board.play(1);
        this.board.play(2);
        this.board.play(6);
        this.board.play(2);
        this.board.play(2);
        this.board.play(3);
        this.board.play(3);
        this.board.play(3);

        assertTrue(this.board.checkWin(3, this.bot1));
    }

}
