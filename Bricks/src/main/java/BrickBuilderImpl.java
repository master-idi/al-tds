import java.util.Arrays;

public class BrickBuilderImpl implements IBrickBuilder {
    private final Integer[] _bricksNumber = new Integer[5];
    private final BrickAbstractFactory _factory;
    private Brick _brick;

    public BrickBuilderImpl(BrickAbstractFactory factory) {
        this._factory = factory;
        Arrays.fill(this._bricksNumber, 0);
    }

    @Override
    public void get1x1() {
        this._brick = this._factory.createBrick1x1();
        ++this._bricksNumber[0];
    }

    @Override
    public void get1x2() {
        this._brick = this._factory.createBrick1x2();
        ++this._bricksNumber[1];
    }

    @Override
    public void get1x4() {
        this._brick = this._factory.createBrick1x4();
        ++this._bricksNumber[3];
    }

    @Override
    public void get2x2() {
        this._brick = this._factory.createBrick2x2();
        ++this._bricksNumber[2];
    }

    @Override
    public void get2x4() {
        this._brick = this._factory.createBrick2x4();
        ++this._bricksNumber[4];
    }

    @Override
    public void moveTo(int x, int y) {
        // ne rien faire
    }

    @Override
    public void rotate(Orientation orientation) {
        // ne rien faire
    }

    @Override
    public void drop() {
        this._brick = null;
    }

    public Integer[] getBricksNumberByBrickType() {
        return _bricksNumber;
    }
}
