public class Brick1x4 extends AbstractBrick {

    public Brick1x4(int _color) {
        super(_color);
    }

    @Override
    public int getWidth() {
        return 1;
    }

    @Override
    public int getHeight() {
        return 4;
    }

    @Override
    public String toString() {
        return "Brick1x4";
    }
}
