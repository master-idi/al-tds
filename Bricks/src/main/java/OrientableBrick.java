public class OrientableBrick {

    private final Brick _brick;
    private Orientation _orientation;
    private Position _position;

    public OrientableBrick(Brick _brick, Orientation _orientation, Position _position) {
        this._brick = _brick;
        this._orientation = _orientation;
        this._position = _position;
    }

    public Brick getBrick() {
        return _brick;
    }

    public Orientation getOrientation() {
        return _orientation;
    }

    public Position getPosition() {
        return _position;
    }

    public void rotate(Orientation orientation) {
        this._orientation = orientation;
    }

    public void moveTo(int x, int y, int z) {
        this._position = new Position(x, y, z);
    }
}
