public class FireFighterBrickFactory implements BrickAbstractFactory{
    private final int _color = 0xFF0000;

    @Override
    public Brick createBrick1x1() {
        return new Brick1x1(this._color);
    }

    @Override
    public Brick createBrick1x2() {
        return new Brick1x2(this._color);
    }

    @Override
    public Brick createBrick1x4() {
        return new Brick1x4(this._color);
    }

    @Override
    public Brick createBrick2x2() {
        return new Brick2x2(this._color);
    }

    @Override
    public Brick createBrick2x4() {
        return new Brick2x4(this._color);
    }
}
