public class Main {

    public static void main(String[] args) {
        double rand = Math.random();
        BrickAbstractFactory factory;
        if (rand <= 0.5) {
            factory = new FireFighterBrickFactory();
        } else {
            factory = new PolicemanBrickFactory();
        }
        Brick brick = factory.createBrick1x1();

        new BrickFactory(brick, 0xFFFFFF);

        System.out.println(brick.toString() + " " + brick.getColor());

        IBrickBuilder builder = new BrickBuilderImpl(factory);

        builder.get1x1();
        builder.get2x2();
        builder.get2x4();
        builder.moveTo(0, 0);
        builder.get1x1();
        builder.get1x2();

        for (int i = 0; i < builder.getBricksNumberByBrickType().length; ++i) {
            System.out.println(i + " : " + builder.getBricksNumberByBrickType()[i]);
        }
    }
}
