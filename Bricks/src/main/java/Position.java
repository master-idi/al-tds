public class Position {
    private final int _posX;
    private final int _posY;
    private final int _posZ;

    public Position(int posX, int posY, int posZ) {
        this._posX = posX;
        this._posY = posY;
        this._posZ = posZ;
    }

    public int getPosX() {
        return _posX;
    }

    public int getPosY() {
        return _posY;
    }

    public int getPosZ() {
        return _posZ;
    }
}
