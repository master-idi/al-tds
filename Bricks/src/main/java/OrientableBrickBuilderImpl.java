import java.util.ArrayList;
import java.util.List;

public class OrientableBrickBuilderImpl implements IBrickBuilder {
    private final List<OrientableBrick> _orientationBricks;
    private OrientableBrick _orientableBrick;

    public OrientableBrickBuilderImpl() {
        this._orientationBricks = new ArrayList<>();
    }

    @Override
    public void get1x1() {
        
    }

    @Override
    public void get1x2() {

    }

    @Override
    public void get1x4() {

    }

    @Override
    public void get2x2() {

    }

    @Override
    public void get2x4() {

    }

    @Override
    public void moveTo(int x, int y) {
        this._orientableBrick.moveTo(x, y, this._orientationBricks.size());
    }

    @Override
    public void rotate(Orientation orientation) {
        this._orientableBrick.rotate(orientation);
    }

    @Override
    public void drop() {
        this._orientableBrick = null;
    }

    @Override
    public Integer[] getBricksNumberByBrickType() {
        return new Integer[0];
    }
}
