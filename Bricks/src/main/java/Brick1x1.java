public class Brick1x1 extends AbstractBrick {

    public Brick1x1(int _color) {
        super(_color);
    }

    @Override
    public int getWidth() {
        return 1;
    }

    @Override
    public int getHeight() {
        return 1;
    }

    @Override
    public String toString() {
        return "Brick1x1";
    }
}
