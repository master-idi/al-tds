public interface IBrickBuilder {
    void get1x1();
    void get1x2();
    void get1x4();
    void get2x2();
    void get2x4();
    void moveTo(int x, int y);
    void rotate(Orientation orientation);
    void drop();

    Integer[] getBricksNumberByBrickType();
}
