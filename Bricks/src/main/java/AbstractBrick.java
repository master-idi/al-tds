public abstract class AbstractBrick implements Brick {
    private final int _color;

    public AbstractBrick(int _color) {
        this._color = _color;
    }

    @Override
    public int getColor() {
         return this._color;
    }

    @Override
    public Brick clone() {
        try {
            return (Brick) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
