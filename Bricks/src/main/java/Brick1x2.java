public class Brick1x2 extends AbstractBrick {

    public Brick1x2(int _color) {
        super(_color);
    }

    @Override
    public int getWidth() {
        return 1;
    }

    @Override
    public int getHeight() {
        return 2;
    }

    @Override
    public String toString() {
        return "Brick1x2";
    }
}
