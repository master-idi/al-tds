public class Brick2x2 extends AbstractBrick {

    public Brick2x2(int _color) {
        super(_color);
    }

    @Override
    public int getWidth() {
        return 2;
    }

    @Override
    public int getHeight() {
        return 2;
    }

    @Override
    public String toString() {
        return "Brick2x2";
    }
}
