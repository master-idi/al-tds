public class BrickFactory implements BrickAbstractFactory {

    private final Brick brick;

    public BrickFactory(Brick brick, int color) {
        this.brick = brick;
    }

    @Override
    public Brick createBrick1x1() {
        return this.brick.clone();
    }

    @Override
    public Brick createBrick1x2() {
        return this.brick.clone();
    }

    @Override
    public Brick createBrick1x4() {
        return this.brick.clone();
    }

    @Override
    public Brick createBrick2x2() {
        return this.brick.clone();
    }

    @Override
    public Brick createBrick2x4() {
        return this.brick.clone();
    }
}
