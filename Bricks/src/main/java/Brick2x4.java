public class Brick2x4 extends AbstractBrick {

    public Brick2x4(int _color) {
        super(_color);
    }

    @Override
    public int getWidth() {
        return 2;
    }

    @Override
    public int getHeight() {
        return 4;
    }

    @Override
    public String toString() {
        return "Brick2x4";
    }
}
