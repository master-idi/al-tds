import model.*;

public class Main {

    public static void main(String[] args) {

        NewReleasePriceCode newReleasePriceCode = new NewReleasePriceCode(3);
        RegularPriceCode childrenPriceCode = new RegularPriceCode(1.5, 1.5, 3);
        RegularPriceCode regularPriceCode = new RegularPriceCode(2, 1.5, 2);

        Movie rogueOne = new Movie("Rogue One");
        Movie frozen = new Movie("Reine des neiges");
        Movie revengeOfTheSith = new Movie("Star Wars III");

        Customers customer = new Customers("John Doe");

        customer.addRental(new Rental(rogueOne, 5, newReleasePriceCode));
        customer.addRental(new Rental(frozen, 7, childrenPriceCode));
        customer.addRental(new Rental(revengeOfTheSith, 4, regularPriceCode));

        System.out.println(customer.getStatement());

    }
}
