package model;

public class Rental {
    private final Movie _movie;
    private final IPriceCode _priceCode;
    private final int _daysRented;

    public Rental(Movie movie, int daysRented, IPriceCode priceCode) {
        this._movie = movie;
        this._daysRented = daysRented;
        this._priceCode = priceCode;
    }

    public Movie getMovie() {
        return this._movie;
    }

    public int getDaysRented() {
        return this._daysRented;
    }

    public double getAmount() {
        return this._priceCode.getAmount(this.getDaysRented());
    }

    public double getRenterPoints() {
        return this._priceCode.getRenterPoint(this.getDaysRented());
    }
}
