package model;

public class NewReleasePriceCode extends RegularPriceCode {

    public NewReleasePriceCode(double price) {
        super(0, price, 0);
    }

    @Override
    public int getRenterPoint(int daysRented) {
        return daysRented > 1 ? 2 : 1;
    }
}
