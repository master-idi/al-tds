package model;

public class Movie {
    private final String _title;

    public Movie(String title) {
        this._title = title;
    }

    public String getTitle() {
        return this._title;
    }
}
