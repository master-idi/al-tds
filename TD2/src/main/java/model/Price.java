package model;

public class Price {

    private final double _value;

    public Price(double value) {
        this._value = value;
    }

    public double getValue() {
        return this._value;
    }
}
