package model;

import java.util.*;

public class Customer {
    private final String _name;
    private final ArrayList<Rental> _rentals;

    public Customer(String name) {
        this._name = name;
        this._rentals = new ArrayList<>();
    }

    public void addRental(Rental rental) {
        this._rentals.add(rental);
    }

    public String getName() {
        return _name;
    }

    public String getStatement() {
        StringBuilder statement = new StringBuilder();
        statement.append("Rental Record for ")
                .append(this.getName())
                .append("\n");

        for (Rental rental : this._rentals) {
            statement.append("\t")
                    .append(rental.getMovie().getTitle())
                    .append("\t")
                    .append(rental.getAmount())
                    .append(" \n");
        }
        statement.append("Amount owned is ")
                .append(this.getRentalsAmount())
                .append("\n")
                .append("You earned ")
                .append(this.getRenterPoints())
                .append(" frequent renter points");

        return statement.toString();
    }

    private double getRentalsAmount() {
        double amount = 0.;
        for (Rental rental : this._rentals) {
            amount += rental.getAmount();
        }
        return amount;
    }

    private int getRenterPoints() {
        int renterPoints = 0;
        for (Rental rental : this._rentals) {
            renterPoints += rental.getRenterPoints();
        }
        return renterPoints;
    }
}
 