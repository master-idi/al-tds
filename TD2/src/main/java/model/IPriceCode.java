package model;

public interface IPriceCode {

    double getAmount(int daysRented);
    int getRenterPoint(int daysRented);
}
