package model;

public class RegularPriceCode implements IPriceCode {

    private final Price _price;
    private final Price _extraPrice;
    private final int _extraDaysLimit;

    public RegularPriceCode(double price, double extraPrice, int extraDaysLimit) {
        this._price = new Price(price);
        this._extraPrice = new Price(extraPrice);
        this._extraDaysLimit = extraDaysLimit;
    }

    @Override
    public double getAmount(int daysRented) {
        if (daysRented < 0) throw new IllegalArgumentException("daysRented cannot be negative.");
        double amount = this._price.getValue();
        int extraDays = Math.max((daysRented - this._extraDaysLimit), 0);
        return amount + extraDays * this._extraPrice.getValue();
    }

    @Override
    public int getRenterPoint(int daysRented) {
        return 1;
    }
}
