package model;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CustomersTest {

    Customers customer;
    @BeforeEach
    void setUp() {

        NewReleasePriceCode newReleasePriceCode = new NewReleasePriceCode(3);
        RegularPriceCode childrenPriceCode = new RegularPriceCode(1.5, 1.5, 3);
        RegularPriceCode regularPriceCode = new RegularPriceCode(2, 1.5, 2);

        Movie rogueOne = new Movie("Rogue One");
        Movie frozen = new Movie("Reine des neiges");
        Movie revengeOfTheSith = new Movie("Star Wars III");

        this.customer = new Customers("John Doe");

        this.customer.addRental(new Rental(rogueOne, 5, newReleasePriceCode));
        this.customer.addRental(new Rental(frozen, 7, childrenPriceCode));
        this.customer.addRental(new Rental(revengeOfTheSith, 4, regularPriceCode));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void statement() {
        assertEquals("Rental Record for John Doe\n" +
                "\tRogue One\t15.0 \n" +
                "\tReine des neiges\t7.5 \n" +
                "\tStar Wars III\t5.0 \n" +
                "Amount owned is 27.5\n" +
                "You earned 4 frequent renter points", customer.getStatement());
    }

    @Test
    void statementNotEqual() {
        assertNotEquals("Rental Record for Aurelien Satger\n" +
                "\tRogue One\t15.0 \n" +
                "\tReine des neiges\t7.5 \n" +
                "\tStar Wars III\t5.0 \n" +
                "Amount owned is 27.5\n" +
                "You earned 4 frequent renter points", customer.getStatement());
    }
}