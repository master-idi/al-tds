# TD2 - Refactoring II

## Exercice I : Remaniement

- Customer.java

- indentation
- nommage
    - Customers -> Customer
    - nommage de variable explicite
    - magic number
- convention de codage
    - indentation (plugin changer le style)
    - nommage des variables
- découpage fonctionnel
    - statements
        - calcul prix loc
        - calcul point fidelite
        - calcul prix total
        - calcul du nombre de point total
        - creation d'un resumé du compte d'un utilisateur
- code vieillissant (Vector avec iterator) on utilise des foreach désormais
- duplication de code
    - les cases dans le switch son dupliqués
    - attention pas toujours évidente
    - `switch` ancêtre du polymorphisme
- parler du nouveau diagramme de class
- parler des aggregation utiliser le pattern prototype possibilite pour un objet de se cloner lui même

## Exercice II : Ajout du format HTML

## Exercice III : Paramétrage du calcul de prix