
package soldier.core;

import soldier.units.UnitVisitor;

public abstract class UnitRider extends UnitSimple {

	public UnitRider(String name, BehaviorSoldier behavior) {
		super(name, behavior);
	}

	@Override
	public void accept(UnitVisitor v) {
		v.visit(this);
	}
}
