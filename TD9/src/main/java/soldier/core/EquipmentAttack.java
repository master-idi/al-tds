
package soldier.core;

import soldier.equipment.EquipmentVisitor;

public abstract class EquipmentAttack extends EquipmentAbstract {

	@Override
	public void accept(EquipmentVisitor v) {
		v.visit(this);
	}

}
