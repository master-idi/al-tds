package soldier.core;

import soldier.units.UnitVisitor;

public class DisplayArmyAttackEquipmentVisitor implements UnitVisitor {
    @Override
    public void visit(UnitGroup u) {

    }

    @Override
    public void visit(UnitInfantry u) {

    }

    @Override
    public void visit(UnitRider u) {

    }

    @Override
    public void visit(EquipmentAttack s) {
        System.out.println(s.getName());
    }

    @Override
    public void visit(EquipmentDefense s) {

    }

    @Override
    public void visit(EquipmentToy s) {

    }
}
