package soldier.core;

import soldier.units.UnitVisitor;

import java.util.HashSet;
import java.util.Set;

public class RiderSupValVisitor implements UnitVisitor {

    private Set<Unit> riders = new HashSet<>();
    private int _val;

    public RiderSupValVisitor(int val) {
        this._val = val;
    }

    @Override
    public void visit(UnitGroup u) {

    }

    @Override
    public void visit(UnitInfantry u) {

    }

    @Override
    public void visit(UnitRider u) {

    }
}
