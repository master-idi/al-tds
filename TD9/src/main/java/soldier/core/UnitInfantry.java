
package soldier.core;

import soldier.units.UnitVisitor;

public abstract class UnitInfantry extends UnitSimple {

	public UnitInfantry(String name, BehaviorSoldier behavior) {
		super(name, behavior);
	}

	@Override
	public void accept(UnitVisitor v) {
		v.visit(this);
	}
}
