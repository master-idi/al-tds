package soldier.core;

import soldier.units.UnitVisitor;

public class ArmyUnitNumberVisitor implements UnitVisitor {

    private int _unitsNumber = 0;

    public int getUnitsNumber() {
        return this._unitsNumber;
    }

    @Override
    public void visit(UnitGroup u) {
    }

    @Override
    public void visit(UnitInfantry u) {
        ++this._unitsNumber;
    }

    @Override
    public void visit(UnitRider u) {
        ++this._unitsNumber;
    }

    @Override
    public void visit(EquipmentAttack s) {

    }

    @Override
    public void visit(EquipmentDefense s) {

    }

    @Override
    public void visit(EquipmentToy s) {

    }
}
