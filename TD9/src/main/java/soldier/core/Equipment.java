
package soldier.core;

import soldier.equipment.EquipmentVisitor;

public interface Equipment extends Cloneable {
	String getName();

	BehaviorSoldier createExtension(BehaviorSoldier s);

	void accept(EquipmentVisitor v);

	Equipment clone();
}
