package soldier.units;

import soldier.core.*;

public interface UnitVisitor {
    void visit(UnitGroup u);
    void visit(UnitInfantry u);
    void visit(UnitRider u);
}
