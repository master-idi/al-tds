
package soldier.units;

public class BehaviorSoldierHealthBased extends BehaviorSoldierStd {
	public BehaviorSoldierHealthBased(float health, float force) {
		super(health, force);
	}

	@Override
	public float strike() {
		return super.strike() * getHealthPoints() / initialHealth();
	}
}
