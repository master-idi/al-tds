/************************
 *
 * @author auber
 *//*


import java.util.ArrayList;
import java.util.Iterator;


*/
/*
 * Work fine without RTTI, runtime type inspection.
 * instanceof
 *//*

interface Element {
    double count();
    double mean();

    void accept(VisitorElement v);
    void affiche();
}

interface VisitorElement {
    void visit(Student std);
    void visit(GroupTD grp);
    void visit(Promo promo);
}

class Student implements Element {
    private double _mean;
    Student(double mean) {
        _mean = mean;
    }
    public void set(double m) {
        _mean = m;
    }
    @Override
    public double count() {
        return 1;
    }
    @Override
    public double mean() {
        return _mean;
    }
    @Override
    public void accept(VisitorElement v) {
        v.visit(this);
    }
    @Override
    public void affiche() {
        System.out.println("Je suis un étudiant avec " + mean() + " de moyenne");

    }
}

abstract class ElementComposite implements Element {
    ArrayList _children = new ArrayList();
    public void addChild(Element e) {
        _children.add(e);
    }
    public Iterator getChildren() {
        return _children.iterator();
    }

    @Override
    public void accept(VisitorElement v) {
        for (Element e : _children) {
            e.accept(v);
        }
    }
    @Override
    public void affiche() {
        for (Element e : _children) {
            e.affiche();
        }
    }

}

abstract class Group extends ElementComposite {
    @Override
    public double count() {
        double sum = 0;
        for (Element e : _children) {
            sum += _children.size();
        }
        return sum;
    }
    @Override
    public double mean() {
        double cnt = 0;
        double sum = 0;
        for (Element e : _children) {
            cnt += e.count();
            sum += e.mean() * e.count();
        }
        if (cnt > 0)
            return sum/cnt;
        else
            return 0;
    }

}

class GroupTD extends Group {
    @Override
    public void addChild(Element e) {
        if(!(e instanceof Student)) throw new UnsupportedOperationException();
        _children.add(e);
    }
    @Override
    public void accept(VisitorElement v) {
        v.visit(this);
        super.accept(v);

    }
    @Override
    public void affiche() {
        System.out.println("Je suis un groupe de td avec " + mean() + " de moyenne");
        super.affiche();
    }

}

class Promo extends Group {
    @Override
    public void addChild(Element e) {
        if(!(e instanceof GroupTD)) throw new UnsupportedOperationException();
        _children.add(e);
    }
    @Override
    public void accept(VisitorElement v) {
        v.visit(this);
        super.accept(v);
    }
    @Override
    public void affiche() {
        System.out.println("Je suis une promo d'étudiant avec " + mean() + " de moyenne");
        super.affiche();
    }
}



class AfficheVisitor implements VisitorElement {
    @Override
    public void visit(Student std) {
        System.out.println("Je suis un étudiant avec " + std.mean() + " de moyenne");
    }

    @Override
    public void visit(GroupTD grp) {
        System.out.println("Je suis un groupe de td avec " + grp.mean() + " de moyenne");
    }

    @Override
    public void visit(Promo promo) {
        System.out.println("Je suis une promo d'étudiant avec " + promo.mean() + " de moyenne");
    }
}

class IncreaseVisitor implements VisitorElement {

    private double _point;

    IncreaseVisitor(double point) {
        _point = point;
    }
    @Override
    public void visit(Student std) {
        std.set(std.mean() + _point);
    }

    @Override
    public void visit(GroupTD grp) {
    }

    @Override
    public void visit(Promo promo) {
    }
}

class NbSupValVisitor implements VisitorElement {

    private double _val;
    public int result = 0;

    public NbSupValVisitor(double val) {
        _val = val;
    }
    @Override
    public void visit(Student std) {
        if (std.mean() > _val)
            ++result;
    }

    @Override
    public void visit(GroupTD grp) {
    }

    @Override
    public void visit(Promo promo) {
    }
}




public class CompositeExemple {

    public static void main(String argv[]) {
        ElementComposite grpA = new GroupTD();
        Element david = new Student(13);
        Element solveig = new Student(19);
        ElementComposite grpB = new GroupTD();
        Element dav = new Student(10);
        Element sol = new Student(9);
        ElementComposite masterGl = new Promo();

        grpA.addChild(solveig);
        grpA.addChild(david);
        grpB.addChild(sol);
        grpB.addChild(dav);
        masterGl.addChild(grpA);
        masterGl.addChild(grpB);

        System.out.println("count : " + masterGl.count());
        System.out.println("mean : " + masterGl.mean());

        masterGl.affiche();

        masterGl.accept(new AfficheVisitor());
        masterGl.accept(new IncreaseVisitor(2));
        masterGl.accept(new AfficheVisitor());

        NbSupValVisitor nbsup = new NbSupValVisitor(11.9);
        masterGl.accept(nbsup);
        System.out.println(" Etudiant avec plus de 12: " + nbsup.result);


    }

}*/
