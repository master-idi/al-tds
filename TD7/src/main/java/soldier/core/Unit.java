package soldier.core;

import java.util.List;

public interface Unit {

    void addEquipment(Equipment equipment);

    float strike();

    float parry(float damage);

}
