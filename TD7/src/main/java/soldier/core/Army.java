package soldier.core;

import java.util.ArrayList;
import java.util.List;

public class Army implements Unit {

    private final List<Unit> units;

    public Army() {
        this.units = new ArrayList<>();
    }

    @Override
    public void addEquipment(Equipment equipment) {
        for (Unit unit : this.units) {
            unit.addEquipment(equipment.clone());
        }
    }

    @Override
    public float strike() {
        float strike = 0;
        for (Unit unit : this.units) {
            strike += unit.strike();
        }
        return strike;
    }

    @Override
    public float parry(float damage) {
        // TODO
        return 0;
    }

    public void add(Unit unit) {
        this.units.add(unit);
    }

    public void remove(Unit unit) {
        this.units.remove(unit);
    }

    public List<Unit> getUnits() {
        return this.units;
    }
}
