
package soldier.units;

import soldier.core.EquipmentException;
import soldier.core.UnitInfantry;
import soldier.core.Equipment;
import soldier.core.UnitObserver;

import java.util.HashSet;

public class UnitCenturion extends UnitInfantry {


	public UnitCenturion(String soldierName) {
		super(soldierName, new BehaviorSoldierStd(15, 100));
	}

	/**
	 * A Centurion can have at most two equipments
	 */
	@Override
	public void addEquipment(Equipment w) {
		if (nbWeapons() > 1)
			throw new EquipmentException();
		super.addEquipment(w);
	}


}
