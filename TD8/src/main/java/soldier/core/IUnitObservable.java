package soldier.core;

public interface IUnitObservable {

    void addObserver(UnitObserver o);

    void removeObserver(UnitObserver o);

    void notifyObservers();
}
