package soldier.core;

public interface UnitObserver {

    void update();
}
