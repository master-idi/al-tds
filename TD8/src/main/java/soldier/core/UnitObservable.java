package soldier.core;

import java.util.HashSet;

public abstract class UnitObservable implements IUnitObservable {

    private final HashSet<UnitObserver> _observers = new HashSet<>();

    @Override
    public void addObserver(UnitObserver o) {
        this._observers.add(o);
    }

    @Override
    public void removeObserver(UnitObserver o) {
        this._observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (UnitObserver observer : this._observers) {
            observer.update();
        }
    }
}