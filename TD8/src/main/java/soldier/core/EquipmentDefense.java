
package soldier.core;

import soldier.equipment.WeaponVisitor;

public abstract class EquipmentDefense extends EquipmentAbstract {

	@Override
	public void accept(WeaponVisitor v) {
		v.visit(this);
	}
}
