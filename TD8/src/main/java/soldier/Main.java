package soldier;

import soldier.core.*;
import soldier.equipment.WeaponShield;
import soldier.units.UnitCenturion;
import soldier.units.UnitHorseMan;

public class Main {

    public static void main(String[] args) {
        UnitObserver obs = new ReportObserver();
        UnitSimple hm = new UnitHorseMan("Horseman");
        UnitSimple im = new UnitCenturion("Centurion");
        Unit gr1 = new UnitGroup("army");
        hm.addObserver(obs);
        im.addObserver(obs);
        //gr1.addObserver(obs);
        gr1.addUnit(hm);
        gr1.addUnit(im);
        UnitSimple hm2 = new UnitCenturion("Centurion 2");
        UnitSimple im2 = new UnitHorseMan("Horsemain 2");
        Unit gr2 = new UnitGroup("army 2");
        hm2.addObserver(obs);
        im2.addObserver(obs);
        //gr2.addObserver(obs);
        gr2.addUnit(hm2);
        gr2.addUnit(im2);
        Unit gr3 = new UnitGroup("army 3");
        gr3.addUnit(gr1);
        gr3.addUnit(gr2);
        //gr3.addObserver(obs);
        Equipment shield = new WeaponShield();
        gr3.addEquipment(shield);
        gr3.parry(gr2.strike());
    }


}
